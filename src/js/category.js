'use strict'
import {categories} from '../helpers/const';

//create categories
function createCategories() {
    categories.forEach(category => {
        let menuList = document.querySelector('.menu-list');
        if(typeof category === "string") {
            let menuListItem = document.createElement('li');
            menuListItem.classList.add('menu-list-item');
            menuListItem.innerHTML = `
                    <a href="#" class="category">${category}</a>`;
            menuList.appendChild(menuListItem);
        } else {
            let children = "";
            category.children.forEach(child => {
                children += `<li><a href="#">${child}</a></li>`;
            });
            let menuListItem = document.createElement('li');
            menuListItem.classList.add('menu-list-item');
            menuListItem.innerHTML = `
                <a href="#" class="category">${category.name}</a>
                <span class="arrow">></span>
                <div class="drop-menu">
                    <ul>
                        <li class="drop-menu-title">Choose</li>
                        ${children}
                    </ul>
                </div>`;
            menuList.appendChild(menuListItem);
        }
    });
};

createCategories();

//collapsed category modal
let openBtn = document.getElementById("collapsed-category-open-btn"),
    closeBtn = document.getElementsByClassName("collapsed-category-close-btn")[0],
    categoryModal = document.getElementById('collapsed-category-modal');

openBtn.addEventListener('click', () => {
    categoryModal.style.display = "flex";
});

closeBtn.addEventListener('click', () => {
    categoryModal.style.display = "none";
});

export function createCollapsedCategories() {
    categories.forEach(category => {
        let collapsedCategoryList = document.querySelector('.collapsed-category-list-item');
        if(typeof category === 'string') {
            let collapsedCategoryListItem = document.createElement('li');
            collapsedCategoryListItem.innerHTML = `<a href="#">${category}</a>`;
            collapsedCategoryList.appendChild(collapsedCategoryListItem);
        } else {
            let collapsedCategoryListItem = document.createElement('li');
            collapsedCategoryListItem.innerHTML = `<a href="#">${category.name}</a>`;
            collapsedCategoryList.appendChild(collapsedCategoryListItem);
        }
    });
};

createCollapsedCategories();
