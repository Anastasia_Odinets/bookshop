'use strict'
import {booksList} from '../helpers/booksList';
import setLoadersTimeout from './loader';
import {searchBook} from "../js/search";

let privateBooks = booksList;

let paging = {
    limit: 16,
    page: 1,
    pageCount: 0
}

//alphabetical sorting
function sortBooks(a, b) {
    let nameA = a.title.toLowerCase(), 
        nameB = b.title.toLowerCase();
    if(nameA < nameB) return -1;
    else if(nameA > nameB) return 1;
    return 0;
}

//render books
export default function createBooksItem(paging) {
    let contentList = document.querySelector('.content-list');
    contentList.innerHTML = "";
    privateBooks.sort(sortBooks);
    for (let i = (paging.page - 1) * paging.limit; i < privateBooks.length && i < paging.page * paging.limit; i++) {
        let book = privateBooks[i],
            contentItemWrap = document.createElement('div');
        contentItemWrap.classList.add('content-item-wrap');
        contentItemWrap.innerHTML = `
        <div class="loader" id="loader">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>                
        </div>
        <div class="content-item">
            <a href="#"><img src="${book.imageLink}" alt="book"></a>
            <a href="#" class="book-title">${book.title}</a>
            <a href="#">${book.author}</a>
            <a href="#">${book.country}</a>
            <a href="#" class="description">${book.description.length > 150
                ? book.description.substring(0, 150) + '...'
                : book.description}</a>
            <div class="cost-wrap">
                <a>${book.cost}</a>
                <button><a href="#">Add to basket</a></button>
            </div>    
        </div> `;
        contentList.appendChild(contentItemWrap);
    }
    setLoadersTimeout();
};

//create pagination buttons
function createPages() {
    let pagination = document.querySelector('.pagination-wrap');
    pagination.innerHTML = "";
    paging.pageCount = privateBooks.length / paging.limit;
    for (let i = 0; i < paging.pageCount; i++) {
        let paginationItem = document.createElement('div');
        paginationItem.classList.add('pagination-style');
        paginationItem.classList.add('page-num');
        paginationItem.textContent = i + 1;
        paginationItem.addEventListener('click', () => {
            createBooksItem({limit: paging.limit, page: i + 1});
            setPage(i);
        });
        pagination.appendChild(paginationItem);
    }   
}

//highlights current page
function setPage(newPageNum) {
    let currentPage = document.querySelector('.current-page');
    if(currentPage != null)
        currentPage.classList.remove('current-page');
    let paginationButtons = document.querySelectorAll(".page-num");
    paginationButtons[newPageNum].classList.add('current-page');
}

createPages();

//perform actual pagination
let nextPageBtn = document.querySelector('.next-page-btn'),
backPageBtn = document.querySelector('.back-page-btn'),
firstPage = document.querySelector('.first-page'),
lastPage = document.querySelector('.last-page');

nextPageBtn.addEventListener('click', () => {
    if(paging.page === paging.pageCount) return;
    paging.page += 1;
    createBooksItem(paging);
    setPage(paging.page - 1);
});

backPageBtn.addEventListener('click', () => {
    if(paging.page === 1) return;
    paging.page -= 1;
    createBooksItem(paging);
    setPage(paging.page - 1);
});

firstPage.addEventListener('click', () => {
    paging.page = 1;
    createBooksItem(paging);
    setPage(paging.page - 1);
});

lastPage.addEventListener('click', () => {
    paging.page = paging.pageCount;
    createBooksItem(paging);
    setPage(paging.page - 1);
});

setPage(0);

//set page limitation
let bookPerPage = document.querySelector('.per-page');

createBooksItem(paging);

 bookPerPage.addEventListener('change', () => {
    let selectedValue = parseInt(bookPerPage.value);
    paging.limit = selectedValue;
    createBooksItem(paging);
    createPages();
    setPage(0);
 });

//search by user input
let inputSearch = document.querySelector('.search-book');

inputSearch.addEventListener('keyup', function (e) {
    let searchResult = document.querySelector('.search-result'),
        wrongSearchResult = document.querySelector('.wrong-search-result');
    searchResult.textContent = '';
    wrongSearchResult.textContent = '';

    if (e.keyCode === 13) {
        privateBooks = searchBook(inputSearch.value);
        searchResult.textContent = 'Result for: ' + '"' + inputSearch.value + '"';

        if(privateBooks.length === 0) {
            wrongSearchResult.textContent = 'Sorry, your search did not match any of the content on site. Make sure all words are spelled correctly, or try different or more general keywords.';
        }
        createBooksItem(paging);
    }
})

//render filtered books
export function renderFilteredBooks(filter) {
    let books = searchBook(filter);
    privateBooks = books;
    createBooksItem(paging);
}

//filtered by cost
export function renderByCost(filteredBooks) {
    privateBooks = filteredBooks;
    createBooksItem(paging);
}