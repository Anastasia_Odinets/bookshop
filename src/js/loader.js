'use strict'
//show loader
function showLoader() {
  let loaders = document.querySelectorAll('.loader'),
    contentItems = document.querySelectorAll('.content-item');
    loaders.forEach(loader => {
      loader.style.display = 'none';
    });
    contentItems.forEach(item => {
      item.style.display = 'flex';
    });
  }
  
export default function setLoadersTimeout() {
    setTimeout(showLoader, 300);
}