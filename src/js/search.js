'use strict'
import {booksList} from '../helpers/booksList';
import {renderFilteredBooks, renderByCost} from '../js/books';

//search book function
export function searchBook(searchCriteria) {
    let filteredBooks = [];
    searchCriteria = searchCriteria.toUpperCase();
    booksList.forEach(book => {
        if(book.author.toUpperCase().indexOf(searchCriteria) > -1 
            || book.title.toUpperCase().indexOf(searchCriteria) > -1 
            || book.country.toUpperCase().indexOf(searchCriteria) > -1
            || book.cost.toUpperCase().indexOf(searchCriteria) > -1) {
            filteredBooks.push(book);
        }
    });

    return filteredBooks;
}

//filter books by author and country
let filterAuthor = document.querySelector('.filter-author'),
    filterCountry = document.querySelector('.filter-country'),
    filterCost = document.querySelector('.filter-cost'),
    filterListAuthor = document.querySelector('.filter-list-author'),
    filterListCountry = document.querySelector('.filter-list-country'),
    filterListCost = document.querySelector('.filter-list-cost');

function renderFilters (filterList, propertyGetter) {
    if(filterList.style.display !=='block') {
        filterList.style.display = 'block';
        if(propertyGetter) {
            let filteredArray = [],
                map = new Map();
            for (let book of booksList) {
                if(!map.has(propertyGetter(book))){
                    map.set(propertyGetter(book), true);
                    filteredArray.push(propertyGetter(book));
                }
            }
            filterList.innerHTML = '';
            filteredArray.sort();
            filteredArray.forEach(filter => {
                let filterListItem = document.createElement('li');
                filterListItem.textContent = `${filter}`;
                filterList.appendChild(filterListItem);
                filterListItem.addEventListener('click', () => {
                    renderFilteredBooks(filter);
                });
            });    
        }   
    } else {
        filterList.style.display = 'none';
    }
}

filterAuthor.addEventListener('click', () => {
    renderFilters(filterListAuthor, b => b.author);
});

filterCountry.addEventListener('click', () => {
    renderFilters(filterListCountry, b => b.country);
});

filterCost.addEventListener('click', () => {
    renderFilters(filterListCost);
});

//filter book by cost
let filterCostLow = document.querySelector('.filter-cost-low'),
    filterCostMedium = document.querySelector('.filter-cost-medium'),
    filterCostHigh = document.querySelector('.filter-cost-high');

 filterCostLow.addEventListener('click', () => {
    filterByCost(0, 9.99);    
});

filterCostMedium.addEventListener('click', () => {
    filterByCost(10, 29.99);    
});

filterCostHigh.addEventListener('click', () => {
    filterByCost(30, 99999);    
});

function filterByCost(min, max) {
    let filteredBooks = [];

    booksList.forEach(book => {
        if(parseInt(book.cost) > min && parseInt(book.cost) < max) {
            filteredBooks.push(book);
            renderByCost(filteredBooks);
        }
    });    
}