export const booksList = [
    {
        "author": "Leo Tolstoy",
        "country": "Russia",
        "imageLink": "https://via.placeholder.com/200/ffeab4",
        "title": "War and Peace",
        "cost": 17 + "$",
        "description": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur harum, labore porro mollitia iure odio ipsa veniam repellat dolore quidem facere accusamus quasi iste voluptatibus aperiam impedit ipsum recusandae eius!"
    },
    {
        "author": "Leo Tolstoy",
        "country": "Russia",
        "imageLink": "https://via.placeholder.com/200/ffeab4",
        "title": "Anna Karenina",
        "cost": 87 + "$",
        "description": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur harum, labore porro mollitia iure odio ipsa veniam repellat dolore quidem facere accusamus quasi iste voluptatibus aperiam impedit ipsum recusandae eius!"
    },
    {
        "author": "Leo Tolstoy",
        "country": "Russia",
        "imageLink": "https://via.placeholder.com/200/ffeab4",
        "title": "The Death of Ivan Ilyich",
        "cost": 12 + "$",
        "description": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur harum, labore porro mollitia iure odio ipsa veniam repellat dolore quidem facere accusamus quasi iste voluptatibus aperiam impedit ipsum recusandae eius!"
    },
    {
        "author": "Mark Twain",
        "country": "United States",
        "imageLink": "https://via.placeholder.com/200/ffeab4",
        "title": "The Adventures of Huckleberry Finn",
        "cost": 18 + "$",
        "description": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur harum, labore porro mollitia iure odio ipsa veniam repellat dolore quidem facere accusamus quasi iste voluptatibus aperiam impedit ipsum recusandae eius!"
    },
    {
        "author": "Valmiki",
        "country": "India",
        "imageLink": "https://via.placeholder.com/200/ffeab4",
        "title": "Ramayana",
        "cost": 11.4 + "$",
        "description": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur harum, labore porro mollitia iure odio ipsa veniam repellat dolore quidem facere accusamus quasi iste voluptatibus aperiam impedit ipsum recusandae eius!"
    },
    {
        "author": "Virgil",
        "country": "Roman Empire",
        "imageLink": "https://via.placeholder.com/200/ffeab4",
        "title": "The Aeneid",
        "cost": 22.64 + "$",
        "description": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur harum, labore porro mollitia iure odio ipsa veniam repellat dolore quidem facere accusamus quasi iste voluptatibus aperiam impedit ipsum recusandae eius!"
    },
    {
        "author": "Vyasa",
        "country": "India",
        "imageLink": "https://via.placeholder.com/200/ffeab4",
        "title": "Mahabharata",
        "cost": 5.19 + "$",
        "description": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur harum, labore porro mollitia iure odio ipsa veniam repellat dolore quidem facere accusamus quasi iste voluptatibus aperiam impedit ipsum recusandae eius!"
    },
    {  
        "author": "Walt Whitman",
        "country": "United States",
        "imageLink": "https://via.placeholder.com/200/ffeab4",
        "title": "Leaves of Grass",
        "cost": 8.25 + "$",
        "description": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur harum, labore porro mollitia iure odio ipsa veniam repellat dolore quidem facere accusamus quasi iste voluptatibus aperiam impedit ipsum recusandae eius!"
    },
    {  
        "author": "Virginia Woolf",
        "country": "United Kingdom",
        "title": "Mrs Dalloway",
        "cost": 7.35 + "$",
        "imageLink": "https://via.placeholder.com/200/ffeab4",
        "description": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur harum, labore porro mollitia iure odio ipsa veniam repellat dolore quidem facere accusamus quasi iste voluptatibus aperiam impedit ipsum recusandae eius!"
    },
    {     
        "author": "Virginia Woolf",
        "country": "United Kingdom",
        "title": "To the Lighthouse",
        "cost": 22 +"$",
        "imageLink": "https://via.placeholder.com/200/ffeab4",
        "description": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur harum, labore porro mollitia iure odio ipsa veniam repellat dolore quidem facere accusamus quasi iste voluptatibus aperiam impedit ipsum recusandae eius!"
    },
    {   
        "author": "Marguerite Yourcenar",
        "country": "France/Belgium",
        "title": "Memoirs of Hadrian",
        "cost": 9.36 + "$",
        "imageLink": "https://via.placeholder.com/200/ffeab4",
        "description": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur harum, labore porro mollitia iure odio ipsa veniam repellat dolore quidem facere accusamus quasi iste voluptatibus aperiam impedit ipsum recusandae eius!"
    },

    {
        "author": "Chinua Achebe",
        "country": "Nigeria",
        "imageLink": "https://via.placeholder.com/200/ffeab4",
        "title": "Things Fall Apart",
        "cost": 25.99 +"$",
        "description": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur harum, labore porro mollitia iure odio ipsa veniam repellat dolore quidem facere accusamus quasi iste voluptatibus aperiam impedit ipsum recusandae eius!"
    },
    {
        "author": "Hans Christian Andersen",
        "country": "Denmark",
        "imageLink": "https://via.placeholder.com/200/ffeab4",
        "title": "Fairy tales",
        "cost": 14.10 +"$",
        "description": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur harum, labore porro mollitia iure odio ipsa veniam repellat dolore quidem facere accusamus quasi iste voluptatibus aperiam impedit ipsum recusandae eius!"
    },
    {
        "author": "Dante Alighieri",
        "country": "Italy",
        "imageLink": "https://via.placeholder.com/200/ffeab4",
        "title": "The Divine Comedy",
        "cost": 11 +"$",
        "description": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur harum, labore porro mollitia iure odio ipsa veniam repellat dolore quidem facere accusamus quasi iste voluptatibus aperiam impedit ipsum recusandae eius!"
    },
    {
        "author": "Unknown",
        "country": "Sumer and Akkadian Empire",
        "imageLink": "https://via.placeholder.com/200/ffeab4",
        "title": "The Epic Of Gilgamesh",
        "cost": 7.50 +"$",
        "description": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur harum, labore porro mollitia iure odio ipsa veniam repellat dolore quidem facere accusamus quasi iste voluptatibus aperiam impedit ipsum recusandae eius!"
        
    },
    {
        "author": "Unknown",
        "country": "Achaemenid Empire",
        "imageLink": "https://via.placeholder.com/200/ffeab4",
        "title": "The Book Of Job",
        "cost": 17 +"$",
        "description": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur harum, labore porro mollitia iure odio ipsa veniam repellat dolore quidem facere accusamus quasi iste voluptatibus aperiam impedit ipsum recusandae eius!"
    }
  ]