export const categories = [
    "Arts & Photography",
    "Biographies & Memoirs",
    "Business & Economics",
    "Children",
    {
        name: "Fiction",
        children : ["Fantasy", "General", "Horror", "Mystery and Crime", "Romance", "Science Fiction"]
    },
    "Comics & Graphic Novels",
    "Computers & Internet",
    "Cooking",
    "Crafts & Hobbies",
    "Diet & Health",
    "Education & Language",
    "Engineering",
    "Entertainment",
    "Games",
    "History"
]