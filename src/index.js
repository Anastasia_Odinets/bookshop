'use strict'
import './styles/reset.scss';
import './styles/main.scss';
import './styles/signUp.scss';
import './js/category';
import './js/books';
import './js/search';
import './js/signUp';